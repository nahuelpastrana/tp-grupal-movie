# Programación Distribuida II

## TP Grupal - Movie

[![pipeline status](https://gitlab.com/inginformaticaundav/pd2/2c2020/tp-grupal-movie/badges/master/pipeline.svg)](https://gitlab.com/inginformaticaundav/pd2/2c2020/tp-grupal-movie/-/commits/master)

### Cómo usar

Clonar el proyecto y ejecutar:
>```sh
>$ docker-compose build
>$ docker-compose up
>```

### Dependencias
Para que la api funcione correctamente, es necesario que se ejecuten los siguientes servicios:

**User:**    [https://gitlab.com/inginformaticaundav/pd2/2c2020/tp-grupal-user]()

**Delivery:**    [https://gitlab.com/inginformaticaundav/pd2/2c2020/delivery]()

**Logging:**     [https://gitlab.com/inginformaticaundav/pd2/2c2020/tp-grupal-logging](url)


### Test
Para ejecutar los tests, se debe correr:
> docker exec pd2-movies-api pytest -v

### Ejemplos

Dentro de la carpeta postman se encuetra una colección de [Postman](https://www.postman.com/downloads/) con ejemplos de llamadas a la API
