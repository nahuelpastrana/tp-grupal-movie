import json
from datetime import datetime, timedelta
from flask_jwt_extended import create_access_token
from src import userClient
from src.repositories import movieRepository
from src.models import Movie, MovieCopy, MovieCopyStatus, MovieInteractionType, MovieInteraction
from src.exceptions import ErrorMessages

def get_mock_header(username="testuser"):
    access_token = create_access_token(username)
    return {
        'Authorization': 'Bearer {}'.format(access_token)
    }

def send_post_json(client, url, data, username="testuser"):
    headers = get_mock_header(username)
    return client.post(
            url,
            data=None if data is None else json.dumps(data),
            content_type='application/json',
            headers=headers
        )

def test_all_movies(client, db): 
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 1)
    newMovie2 = Movie("Movie 2", "Category 2", "Actor 2", 2)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    newMovie2.copies.append(MovieCopy(newMovie2.movieId))
    movieRepository.saveChanges([newMovie1,newMovie2])
    headers = get_mock_header()

    response = client.get("/movie/",headers=headers)
    data = json.loads(response.data.decode())
    assert len(data) == 2
    assert response.status_code == 200
    

def test_get_movie(client, db):  
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 1)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])
    #########
    username="HOMERO_S"  
    headers = get_mock_header(username)

    response = client.get("/movie/{}".format(newMovie1.movieId),headers=headers)
    data = json.loads(response.data.decode())

    movie = movieRepository.getById(newMovie1.movieId)

    assert response.status_code == 200
    assert newMovie1.title == data['title']
    assert newMovie1.category == data['category']
    assert newMovie1.price == data['price']
    assert len(data['actors']) == 1
    assert newMovie1.actor == data['actors'][0]
    assert len(data['copies']) == 1
    assert newMovie1.copies[0].movieCopyId == data['copies'][0]['id']
    assert newMovie1.copies[0].status == data['copies'][0]['status']
    
    assert len(movie.interactions) == 1
    assert movie.interactions[0].type == MovieInteractionType.CONSULTED
    assert movie.interactions[0].username == username
    assert movie.interactions[0].date is not None
    assert movie.interactions[0].parentId is None

def test_rent(client, db, mocker): 
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 10)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])
    #mocks
    mocker.patch('src.UserClient.getCreditCurrentUser',return_value=30)
    mocker.patch('src.UserClient.processPayment')
    mocker.patch('src.DeliveryClient.anyDelivery',return_value=True)
    mocker.patch('src.DeliveryQueuesClient.send_rent')
    mocker.patch('src.DeliveryQueuesClient.connect')
    mocker.patch('src.DeliveryQueuesClient.check_connection')

    #########
    username="HOMERO_S"  
    response = send_post_json(client, "/movie/{}/rent".format(newMovie1.movieId), None, username)
    data = json.loads(response.data.decode())
    movie = movieRepository.getById(newMovie1.movieId)

    assert response.status_code == 200
    assert "message" in data
    assert len(movie.copies) == 1
    assert movie.copies[0].status == MovieCopyStatus.RENTED
    assert movie.copies[0].rentedFrom is not None
    assert len(movie.interactions) == 1
    assert movie.interactions[0].type == MovieInteractionType.RENTED
    assert movie.interactions[0].username == username
    assert movie.interactions[0].date is not None
    assert movie.interactions[0].parentId is None


def test_rent_insufficient_credit(client, db, mocker):   
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 100)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])
    #mocks
    mocker.patch('src.UserClient.getCreditCurrentUser',return_value=2)
    mocker.patch('src.DeliveryClient.anyDelivery',return_value=True)
    #########
    username="HOMERO_S"  
    response = send_post_json(client, "/movie/{}/rent".format(newMovie1.movieId), None, username)
    data = json.loads(response.data.decode())

    movie = movieRepository.getById(newMovie1.movieId)

    assert response.status_code == 400
    assert "errors" in data
    assert data["errors"] == ErrorMessages.NO_CREDIT

    assert len(movie.interactions) == 1
    assert movie.interactions[0].type == MovieInteractionType.NO_CREDIT
    assert movie.interactions[0].username == username
    assert movie.interactions[0].date is not None
    assert movie.interactions[0].parentId is None

def test_rent_insufficient_stock(client, db, mocker):   
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 100)
    copy = MovieCopy(newMovie1.movieId)
    copy.markAsRented()
    newMovie1.copies.append(copy)
    movieRepository.saveChanges([newMovie1])
    #mocks
    mocker.patch('src.UserClient.getCreditCurrentUser',return_value=9000)
    mocker.patch('src.DeliveryClient.anyDelivery',return_value=True)
    #########
    username="HOMERO_S"  
    response = send_post_json(client, "/movie/{}/rent".format(newMovie1.movieId), None, username)
    data = json.loads(response.data.decode())

    movie = movieRepository.getById(newMovie1.movieId)

    assert response.status_code == 400
    assert "errors" in data
    assert data["errors"] == ErrorMessages.NO_STOCK

    assert len(movie.interactions) == 1
    assert movie.interactions[0].type == MovieInteractionType.NO_STOCK
    assert movie.interactions[0].username == username
    assert movie.interactions[0].date is not None
    assert movie.interactions[0].parentId is None

def test_rent_insufficient_delivery(client, db, mocker):   
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 100)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])
    #mocks
    mocker.patch('src.UserClient.getCreditCurrentUser',return_value=9000)
    mocker.patch('src.DeliveryClient.anyDelivery',return_value=False)
    #########
    username="HOMERO_S"  
    response = send_post_json(client, "/movie/{}/rent".format(newMovie1.movieId), None, username)
    data = json.loads(response.data.decode())

    movie = movieRepository.getById(newMovie1.movieId)

    assert response.status_code == 400
    assert "errors" in data
    assert data["errors"] == ErrorMessages.NO_DELIVERY

    assert len(movie.interactions) == 1
    assert movie.interactions[0].type == MovieInteractionType.NO_DELIVERY
    assert movie.interactions[0].username == username
    assert movie.interactions[0].date is not None
    assert movie.interactions[0].parentId is None

def test_rent_movie_not_found(client, db, mocker):   
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 100)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])
    #mocks
    mocker.patch('src.UserClient.getCreditCurrentUser',return_value=9000)
    mocker.patch('src.DeliveryClient.anyDelivery',return_value=False)
    #########
    username="HOMERO_S"  
    response = send_post_json(client, "/movie/{}/rent".format(2), None, username)

    assert response.status_code == 404

def test_check_availability_movie_available(client, db, mocker):   
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 100)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])
    #########
    headers = get_mock_header()  
    response = client.get("/movie/{}/availability".format(newMovie1.movieId), headers=headers)
    data = json.loads(response.data.decode())

    assert response.status_code == 200
    assert "date" in data
    assert data["date"] is not None

def test_check_availability_movie_available(client, db, mocker):   
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 100)
    copy1 = MovieCopy(newMovie1.movieId)
    copy2 = MovieCopy(newMovie1.movieId)
    copy3 = MovieCopy(newMovie1.movieId)
    copy1.markAsRented()
    copy2.markAsRented()
    copy3.markAsRented()
    copy1.rentedFrom=copy1.rentedFrom + timedelta(hours=-4) # la menos antigua, "se alquiló hace 4hs"
    copy2.rentedFrom=copy2.rentedFrom + timedelta(days=-1) # la más antigua, "se alquiló ayer"
    copy3.rentedFrom=copy3.rentedFrom + timedelta(hours=-6)# "se alquiló hace 6hs"
    newMovie1.copies.append(copy1)
    newMovie1.copies.append(copy2)
    newMovie1.copies.append(copy3)
    movieRepository.saveChanges([newMovie1])
    #########
    dateExpected= copy2.rentedFrom + timedelta(days=2)
    headers = get_mock_header()  
    response = client.get("/movie/{}/availability".format(newMovie1.movieId), headers=headers)
    data = json.loads(response.data.decode())

    assert response.status_code == 200
    assert "date" in data
    assert data["date"] is not None
    assert data["date"] == dateExpected.isoformat()

def test_check_availability_no_found(client, db, mocker):   
    #data init
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 100)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])
    #########
    headers = get_mock_header()  
    response = client.get("/movie/{}/availability".format(23), headers=headers)

    assert response.status_code == 404
    
def test_create_movie(client, db):
    title = "El rey leon 5"
    category = "Drama"
    actor = "Mufasa"
    price = 200
    formData = dict(title=title,category=category,actor=actor,price=price)
    response = send_post_json(client, "/movie/",formData)
    data = json.loads(response.data.decode())
    movies = movieRepository.find()

    assert response.status_code == 201
    assert len(movies) == 1
    assert movies[0].title == data['title'] == title
    assert movies[0].category == data['category'] == category
    assert movies[0].price == data['price'] == price
    assert len(data['actors']) == 1
    assert movies[0].actor == data['actors'][0] == actor
    assert len(data['copies']) == 1
    assert movies[0].copies[0].movieCopyId == data['copies'][0]['id']
    assert movies[0].copies[0].status == data['copies'][0]['status']
    assert len(movies[0].interactions) == 0

def test_create_movie_unique_title(client, db):
    title = "El rey leon 5" 
    #data init
    newMovie1 = Movie(title, "Category 1", "Actor 1", 100)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])
    ############
    formData = dict(title=title,category="Drama",actor="Mufasa",price=212)
    response = send_post_json(client, "/movie/",formData)
    data = json.loads(response.data.decode())

    assert response.status_code == 400
    assert "errors" in data
    assert data["errors"] == ErrorMessages.MOVIE_TITLE_ALREADY_EXISTS

def test_create_movie_validate_input(client, db):
    formData = dict(title="El rey leon 5",category="Drama",actor="Mufasa",price="ads")
    response = send_post_json(client, "/movie/",formData)
    data = json.loads(response.data.decode())

    assert response.status_code == 400
    assert "errors" in data

def test_get_most_rented(client, db):
    newMovie = Movie("Movie 1", "Category 1", "Actor 1", 1)
    newMovie.copies.append(MovieCopy(newMovie.movieId))
    movieRepository.saveChanges([newMovie])

    username = "Gaston"
    newMovieInteraction = MovieInteraction.createRented(newMovie, username)
    movieRepository.saveChanges([newMovieInteraction])

    headers = get_mock_header()
    from_date=  (datetime.now()  + timedelta(days=-2)).strftime('%Y-%m-%d')
    until_date= (datetime.now()  + timedelta(days=+2)).strftime('%Y-%m-%d')
    response = client.get("/movie/metrics/most-rented?from={}&until={}".format(from_date,until_date), headers=headers)
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert len(data) == 1
    assert data[0]['count'] == 1
    assert data[0]['movieId'] == newMovie.movieId
    assert 'RENTED' == newMovieInteraction.type

def test_get_most_rented_invalid_date(client, db):
    headers = get_mock_header()
    from_date=  "2020-01-01"    
    until_date= "2020-12-xx"
    response = client.get("/movie/metrics/most-rented?from={}&until={}".format(from_date,until_date), headers=headers)
    data = json.loads(response.data.decode())
    
    assert response.status_code == 400
    assert "errors" in data
    assert data["errors"] == '{} no es una fecha con formato YYYY-MM-DD'.format(until_date)

def test_get_failed_attempts(client, db):
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 100)
    newMovie2 = Movie("Movie 2", "Category 2", "Actor 2", 100)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    copy = MovieCopy(newMovie2.movieId)
    copy.markAsRented()
    newMovie2.copies.append(copy)
    movieRepository.saveChanges([newMovie1, newMovie2])

    username = "Pepe"
    newMovieInteraction1 = MovieInteraction.createNoCredit(newMovie1, username)
    movieRepository.saveChanges([newMovieInteraction1])
    username = "Pipo"
    newMovieInteraction2 = MovieInteraction.createNoDelivery(newMovie1, username)
    movieRepository.saveChanges([newMovieInteraction2])
    username = "Miguel"
    newMovieInteraction3 = MovieInteraction.createNoStock(newMovie2, username)
    movieRepository.saveChanges([newMovieInteraction3])
    headers = get_mock_header()
    from_date=  (datetime.now()  + timedelta(days=-2)).strftime('%Y-%m-%d')
    until_date= (datetime.now()  + timedelta(days=+2)).strftime('%Y-%m-%d')   
    response = client.get("/movie/metrics/failed-attempts?from={}&until={}".format(from_date,until_date), headers=headers)
    data = json.loads(response.data.decode())

    assert response.status_code == 200
    assert len(data) == 2
    assert data[0]['count'] == 2
    assert data[1]['count'] == 1
    assert data[0]['movieId'] == newMovie1.movieId
    assert data[1]['movieId'] == newMovie2.movieId 
    assert newMovieInteraction1.type == MovieInteractionType.NO_CREDIT
    assert newMovieInteraction2.type == MovieInteractionType.NO_DELIVERY
    assert newMovieInteraction3.type == MovieInteractionType.NO_STOCK
    assert newMovieInteraction1.username == 'Pepe'
    assert newMovieInteraction2.username == 'Pipo'
    assert newMovieInteraction3.username == 'Miguel'
    assert newMovieInteraction1.date is not None
    assert newMovieInteraction2.date is not None
    assert newMovieInteraction3.date is not None

def test_get_failed_attempts_invalid_date(client, db):
    headers = get_mock_header()
    from_date=  "2020-01-01"    
    until_date= "20xx-12-31"
    response = client.get("/movie/metrics/failed-attempts?from={}&until={}".format(from_date,until_date), headers=headers)
    data = json.loads(response.data.decode())
    
    assert response.status_code == 400
    assert "errors" in data
    assert data["errors"] == '{} no es una fecha con formato YYYY-MM-DD'.format(until_date)

def test_get_days_hours_inquiries(client, db):
    newMovie = Movie("Movie 1", "Category 1", "Actor 1", 1)
    newMovie.copies.append(MovieCopy(newMovie.movieId))
    movieRepository.saveChanges([newMovie])

    username="juan"  
    newMovieInteraction = MovieInteraction.createConsulted(newMovie, username)
    movieRepository.saveChanges([newMovieInteraction])

    headers = get_mock_header()
    from_date=  (datetime.now()  + timedelta(days=-2)).strftime('%Y-%m-%d')
    until_date= (datetime.now()  + timedelta(days=+2)).strftime('%Y-%m-%d')     

    response = client.get("/movie/metrics/days-hours-inquiries?from={}&until={}".format(from_date,until_date), headers=headers)
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert len(data) == 1
    assert data[0]['count'] == 1
    assert data[0]['day'] == newMovieInteraction.date.strftime('%A')
    assert data[0]['hour'] == newMovieInteraction.date.hour
    assert 'CONSULTED' == newMovieInteraction.type

def test_get_days_hours_inquiries_invalid_date(client, db):
    headers = get_mock_header()
    from_date=  "2020-01-0x"    
    until_date= "2020-12-31"
    response = client.get("/movie/metrics/days-hours-inquiries?from={}&until={}".format(from_date,until_date), headers=headers)
    data = json.loads(response.data.decode())
    
    assert response.status_code == 400
    assert "errors" in data
    assert data["errors"] == '{} no es una fecha con formato YYYY-MM-DD'.format(from_date)

def test_get_history_inquiries(client, db):
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 1)
    newMovie2 = Movie("Movie 2", "Category 2", "Actor 2", 2)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    newMovie2.copies.append(MovieCopy(newMovie2.movieId))
    movieRepository.saveChanges([newMovie1,newMovie2])

    newMovieInteraction1 = MovieInteraction.createConsulted(newMovie1, 'testuser')
    newMovieInteraction2 = MovieInteraction.createConsulted(newMovie2, 'testuser')
    movieRepository.saveChanges([newMovieInteraction1, newMovieInteraction2])

    headers = get_mock_header()      
    response = client.get("/movie/history/inquiries",headers=headers)
    data = json.loads(response.data.decode())

    assert response.status_code == 200
    assert len(data) == 2
    assert data[0]['movieId'] == newMovie1.movieId
    assert data[0]['title'] == newMovie1.title
    assert data[0]['username'] == newMovieInteraction1.username
    assert data[1]['movieId'] == newMovie2.movieId
    assert data[1]['title'] == newMovie2.title
    assert data[1]['username'] == newMovieInteraction2.username
    assert data[0]["date"] is not None
    assert data[1]["date"] is not None

def test_get_history_no_credit(client, db):
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 1)
    newMovie2 = Movie("Movie 2", "Category 2", "Actor 2", 2)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    newMovie2.copies.append(MovieCopy(newMovie2.movieId))
    movieRepository.saveChanges([newMovie1,newMovie2])

    username1 = "Pepe"
    username2 = "Pipo"
    newMovieInteraction1 = MovieInteraction.createNoCredit(newMovie1, username1)
    newMovieInteraction2 = MovieInteraction.createNoCredit(newMovie2, username2)
    movieRepository.saveChanges([newMovieInteraction1, newMovieInteraction2])

    headers = get_mock_header()      
    response = client.get("/movie/history/no-credit",headers=headers)
    data = json.loads(response.data.decode())

    assert response.status_code == 200
    assert len(data) == 2
    assert data[0]['movieId'] == newMovie1.movieId
    assert data[0]['title'] == newMovie1.title
    assert data[0]['username'] == newMovieInteraction1.username
    assert data[1]['movieId'] == newMovie2.movieId
    assert data[1]['title'] == newMovie2.title
    assert data[1]['username'] == newMovieInteraction2.username
    assert data[0]["date"] is not None
    assert data[1]["date"] is not None

def test_get_history_rents(client, db):
    newMovie1 = Movie("Movie 1", "Category 1", "Actor 1", 10)
    newMovie1.copies.append(MovieCopy(newMovie1.movieId))
    movieRepository.saveChanges([newMovie1])

    newMovieInteraction1 = MovieInteraction.createRented(newMovie1, 'testUser')
    movieRepository.saveChanges([newMovieInteraction1])
    newMovieInteraction2 = MovieInteraction.createAssignedDeliver(newMovieInteraction1, 'testDelivery')
    movieRepository.saveChanges([newMovieInteraction2])

    headers = get_mock_header() 
    from_date=  (datetime.now()  + timedelta(days=-2)).strftime('%Y-%m-%d')
    until_date= (datetime.now()  + timedelta(days=+2)).strftime('%Y-%m-%d')    
    response = client.get("/movie/history/rents?from={}&until={}".format(from_date,until_date), headers=headers)
    data = json.loads(response.data.decode())

    assert response.status_code == 200
    assert len(data) == 1  
    assert newMovieInteraction2.parentId == newMovieInteraction1.movieInteractionId
    assert newMovieInteraction2.type == MovieInteractionType.ASSIGNED_DELIVERY
    assert data[0]['deliveryBy'] == newMovieInteraction2.username
    assert data[0]['deliveryDate'] == newMovieInteraction2.date.isoformat()
    assert data[0]['movieId'] == newMovie1.movieId
    assert data[0]["rentBy"] == newMovieInteraction1.username
    assert data[0]["rentDate"] == newMovieInteraction1.date.isoformat()
    assert data[0]["title"] == newMovie1.title 

