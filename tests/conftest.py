import ctypes
from os import environ, path
import contextlib
from dotenv import load_dotenv
import pytest
import sqlalchemy as sa
from flask_jwt_extended import JWTManager
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from packaging import version
from pytest_postgresql.factories import DatabaseJanitor
from src import create_app, db as _db
from src.models import Movie, MovieCopy, insert_initial_values_movies, insert_initial_values_movies_copies

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

DB_CONN = environ.get("SQLALCHEMY_DATABASE_URI_TEST")
DB_OPTS = sa.engine.url.make_url(DB_CONN).translate_connect_args()

# Se quitan la data iniciales de la app, para poder tener control total en los unit test, de que hay
sa.event.remove(Movie.__table__, 'after_create',insert_initial_values_movies)
sa.event.remove(MovieCopy.__table__, 'after_create',insert_initial_values_movies_copies)

@pytest.fixture(scope='session')
def database(request):
    pg_host = DB_OPTS.get("host")
    pg_port = DB_OPTS.get("port")
    pg_user = DB_OPTS.get("username")
    pg_password = DB_OPTS.get("password")
    db_name = DB_OPTS.get("database")

    janitor = DatabaseJanitor(pg_user, pg_host, pg_port, db_name, 9.6, pg_password)

    try:
        janitor.init()
    except:
        janitor.drop()
        janitor.init()

    @request.addfinalizer
    def drop_database():
        janitor.drop()

@pytest.fixture
def app(database):
    app = create_app("ConfigTest")
    yield app

@pytest.fixture
def client(app):
    with app.test_client() as client:
        with app.app_context():
            _db.drop_all()
            _db.create_all()
        yield client


@pytest.fixture
def db(app):
    app.app_context().push()
    _db.init_app(app)
    _db.create_all()
    yield _db
    _db.session.close()
    _db.drop_all()


    

