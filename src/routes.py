from flask import Blueprint, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from jsonschema import ValidationError
from datetime import datetime, timedelta
import logging

from . import utls
from .repositories import movieRepository
from .dtos import UserMovieInteractionDTO, MovieDTO, UserMovieInteractionDeliveryDTO
from .models import MovieInteractionType, Movie
from .exceptions import BusinessException, ExternalServiceUnavailableException
from .services import movieService

healthcheck = Blueprint("healthcheck-api", __name__, url_prefix="/healthcheck/")
movies = Blueprint("movie-api", __name__, url_prefix="/movie/")
histories = Blueprint("movie-history-api", __name__, url_prefix="/movie/history")
metrics = Blueprint("movie-metrics-api", __name__, url_prefix="/movie/metrics")

def route_error(app):
    @app.errorhandler(Exception)
    def exception_handler(e):
        logging.error(str(e))
        return utls.response_error(str(e), 500)

    @app.errorhandler(ExternalServiceUnavailableException)
    def external_service_exception_handler(e):
        logging.error(str(e))
        return utls.response_error(e.message, 503)

    @app.errorhandler(BusinessException)
    def business_exception_handler(e):
        return utls.responseBusinessError(e.message)

    @app.errorhandler(ValidationError)
    def exception_validation_handler(e):
        return utls.responseBusinessError(e.message)

@movies.route("/",methods=['GET'])
@jwt_required
def allMovies():
    validKeys= ['title', 'actor', 'category']
    for key in list(request.args):
        if key not in validKeys:
            return utls.responseBusinessError("Parametro de busqueda no valido")
    title = request.args.get('title', type = str)
    actor = request.args.get('actor', type = str)
    category = request.args.get('category', type = str)
    movies = movieRepository.findLike(title = title, actor = actor, category = category,order_by=Movie.movieId.desc())
    result = list(map(lambda x: MovieDTO(x).serialize(), movies))
    return utls.responseJson(result)

@movies.route("/<movie_id>",methods=['GET'])
@jwt_required
def getMovie(movie_id):
    movie = movieRepository.getById(movie_id)
    if(movie is None):
        return utls.responseNotFound("No se encontró la pelicula")
    movieService.registerInquiry(movie, get_jwt_identity())
    return utls.responseJson(MovieDTO(movie).serialize())

@movies.route("/<movie_id>/availability",methods=['GET'])
@jwt_required
def checkAvailability(movie_id):
    movie = movieRepository.getById(movie_id)
    if(movie is None):
        return utls.responseNotFound("No se encontró la pelicula")
    result = movieService.getAvailabilityDate(movie)
    return utls.responseJson(result)

@movies.route("/",methods=['POST'])
@jwt_required
@utls.only_json
def createMovie():
    newMovie = movieService.create(request.json_data)
    return utls.responseJson(newMovie, 201)

@movies.route("/<movie_id>/rent",methods=['POST'])
@jwt_required
def create_rent(movie_id):
    movie = movieRepository.getById(movie_id)
    if(movie is None):
        return utls.responseNotFound("No se encontró la pelicula")
    movieService.rent(movie, get_jwt_identity())
    return utls.responseMessage("El alquiler se realizó con éxito")

@metrics.route("/most-rented",methods=['GET'])
@jwt_required
def get_most_rented():
    date_from = request.args.get('from', default = (datetime.now() - timedelta(weeks=4)).date(), type = utls.paramToDate)
    date_until = request.args.get('until', default = datetime.now().date(), type = utls.paramToDate)
    interactions = movieRepository.groupByCountRentedInteractionsByMovieId(date_from=date_from, date_until=date_until)
    result = movieService.getMovieIdAndCountlist(interactions)
    return utls.responseJson(result)

@metrics.route("/failed-attempts",methods=['GET'])
@jwt_required
def get_failed_attempts():
    date_from = request.args.get('from', default = (datetime.now() - timedelta(weeks=4)).date(), type = utls.paramToDate)
    date_until = request.args.get('until', default = datetime.now().date(), type = utls.paramToDate)
    interactions = movieRepository.groupByCountFailedsInteractionsByMovieId(date_from=date_from, date_until=date_until)
    result = movieService.getMovieIdAndCountlist(interactions)
    return utls.responseJson(result)

@metrics.route("/days-hours-rents",methods=['GET'])
@jwt_required
def rents_for_day_hours():
    date_from = request.args.get('from', default = (datetime.now() - timedelta(weeks=4)).date(), type = utls.paramToDate)
    date_until = request.args.get('until', default = datetime.now().date(), type = utls.paramToDate)
    result = movieService.getRentsForDayHours(date_from, date_until)
    return utls.responseJson(result)

@metrics.route("/days-hours-inquiries",methods=['GET'])
@jwt_required
def get_days_hours_inquiries():
    date_from = request.args.get('from', default = (datetime.now() - timedelta(weeks=4)).date(), type = utls.paramToDate)
    date_until = request.args.get('until', default = datetime.now().date(), type = utls.paramToDate)
    result = movieService.getDaysHoursInquiries(date_from, date_until)
    return utls.responseJson(result)

@histories.route("/inquiries",methods=['GET'])
def get_history_inquiries():
    inquiries = movieRepository.findInteractions(type=MovieInteractionType.CONSULTED)
    result = list(map(lambda x: UserMovieInteractionDTO(x).serialize(), inquiries))
    return utls.responseJson(result)

@histories.route("/no-credit",methods=['GET'])
def get_history_no_credit():
    noCredit = movieRepository.findInteractions(type=MovieInteractionType.NO_CREDIT)
    result = list(map(lambda x: UserMovieInteractionDTO(x).serialize(), noCredit))
    return utls.responseJson(result)

@histories.route("/rents",methods=['GET'])
def get_history_rents():
    date_from = request.args.get('from', default = (datetime.now() - timedelta(weeks=4)).date(), type = utls.paramToDate)
    date_until = request.args.get('until', default = datetime.now().date(), type = utls.paramToDate)
    interactions = movieRepository.findInteractions(type=MovieInteractionType.ASSIGNED_DELIVERY, date_from=date_from, date_until=date_until)
    result = list(map(lambda x: UserMovieInteractionDeliveryDTO(x).serialize(), interactions))
    return utls.responseJson(result)

@healthcheck.route("/",methods=['GET'])
def get_healthcheck_status():
    return utls.responseJson({"status":"ok"})