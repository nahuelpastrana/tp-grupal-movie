import json

class UserMovieInteractionDTO():
    def __init__(self, movieInteraction): 
        self.mi = movieInteraction
    def serialize(self):
        return {
            "username": self.mi.username,
            "date": self.mi.date,
            "movieId": self.mi.movie.movieId,
            "title": self.mi.movie.title
        }

class UserMovieInteractionDeliveryDTO():
    def __init__(self, movieInteraction): 
        self.mi = movieInteraction
    def serialize(self):
        return {
            "movieId": self.mi.movie.movieId,
            "title": self.mi.movie.title,
            "rentBy": self.mi.parent.username,
            "rentDate": self.mi.parent.date,
            "deliveryBy": self.mi.username,
            "deliveryDate": self.mi.date
        }

class MovieCopyDTO():
    def __init__(self, movieCopy): 
        self.mc = movieCopy
    
    def serialize(self):
        return {
            "id": self.mc.movieCopyId, 
            "status": self.mc.status,
            "rentedFrom": self.mc.rentedFrom
        }

class MovieDTO():
    def __init__(self, movie): 
        self.movie = movie
    def serialize(self):
        copies= list(map(lambda x: MovieCopyDTO(x).serialize(), self.movie.copies))
        return {
            "id": self.movie.movieId,
            "title": self.movie.title,
            "category": self.movie.category,
            "actors": self.movie.actor.split(";"),
            "price": self.movie.price,
            "copies": copies
        }

class AvailabilityDateDTO():
    def __init__(self, date): 
        self.date=date
    def serialize(self):
        return { "date": self.date}

class MovieCountDTO():
    def __init__(self, movieId, count): 
        self.movieId=movieId
        self.count=count
    def serialize(self):
        return {
            "movieId": self.movieId,
            "count": self.count
        }

class DayHourCountDTO():
    def __init__(self, day, hour, count): 
        self.day=day
        self.hour=hour
        self.count=count
    def serialize(self):
        return {
            "day": self.day,
            "hour": self.hour,
            "count": self.count
        }