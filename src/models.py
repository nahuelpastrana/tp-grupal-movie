from enum import Enum
from sqlalchemy_serializer import SerializerMixin
from datetime import datetime
from sqlalchemy import event, Enum
from . import db

class Movie(db.Model):
    __tablename__ = "movies"
    def __init__(self, title, category, actor, price): 
        self.title = title
        self.category = category
        self.actor = actor
        self.price = price
    
    movieId = db.Column('movie_id', db.Integer, primary_key=True)
    title = db.Column(db.String(200), nullable=False, unique= True)
    category = db.Column(db.String(100), nullable=False)
    actor = db.Column(db.String(1000), nullable=False)
    price = db.Column(db.Float, nullable=False)    

    copies = db.relationship('MovieCopy', backref='movie')
    interactions = db.relationship('MovieInteraction', backref='movie')

    def __repr__(self):
        return "<Movies {}>".format(self.movie_id)

class MovieCopy(db.Model):
    __tablename__ = "movies_copies"
    def __init__(self, movieId): 
        self.movieId = movieId
        self.status = MovieCopyStatus.AVAILABLE
    movieCopyId = db.Column('movie_copy_id',db.Integer, primary_key=True)
    movieId = db.Column('movie_id', db.Integer, db.ForeignKey('movies.movie_id'), nullable=False)
    status = db.Column(db.String(200), nullable=False)
    rentedFrom = db.Column('rented_from', db.DateTime, nullable=True)

    def markAsRented(self):
        self.status = MovieCopyStatus.RENTED
        self.rentedFrom = datetime.now()

    def __repr__(self):
        return "<MovieCopy {}>".format(self.movieCopyId)

class MovieInteraction(db.Model):
    __tablename__ = "movies_interactions"
    def __init__(self, movieId, username, type, parentId): 
        self.movieId = movieId
        self.username = username
        self.date = datetime.now()
        self.type = type
        self.parentId = parentId
    
    movieInteractionId = db.Column('movie_interaction_id',db.Integer, primary_key=True)
    movieId = db.Column('movie_id',db.Integer, db.ForeignKey('movies.movie_id'), nullable=False)
    username = db.Column('username',db.String(50), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    type= db.Column(db.String(200), nullable=False)
    parentId = db.Column('movie_interaction_parent_id',db.Integer, db.ForeignKey('movies_interactions.movie_interaction_id'), nullable=True)
    parent = db.relationship('MovieInteraction', remote_side=[movieInteractionId])
    
    @staticmethod
    def createRented(movie, username):
        return MovieInteraction(movie.movieId, username, MovieInteractionType.RENTED, None)
    
    @staticmethod
    def createConsulted(movie, username):
        return MovieInteraction(movie.movieId, username, MovieInteractionType.CONSULTED, None)
    
    @staticmethod
    def createNoStock(movie, username):
        return MovieInteraction(movie.movieId, username, MovieInteractionType.NO_STOCK, None)

    @staticmethod
    def createNoCredit(movie, username):
        return MovieInteraction(movie.movieId, username, MovieInteractionType.NO_CREDIT, None)

    @staticmethod
    def createNoDelivery(movie, username):
        return MovieInteraction(movie.movieId, username, MovieInteractionType.NO_DELIVERY, None)

    @staticmethod
    def createAssignedDeliver(parent, username):
        return MovieInteraction(parent.movieId, username, MovieInteractionType.ASSIGNED_DELIVERY, parent.movieInteractionId)

    def __repr__(self):
        return "<MovieInteraction {}>".format(self.movie_interaction_Id)

class MovieCopyStatus(Enum):
    AVAILABLE="AVAILABLE"
    RENTED="RENTED"

class MovieInteractionType(Enum):
    RENTED="RENTED"
    NO_DELIVERY="NO_DELIVERY"
    NO_CREDIT="NO_CREDIT"
    NO_STOCK="NO_STOCK"
    CONSULTED="CONSULTED"
    ASSIGNED_DELIVERY="ASSIGNED_DELIVERY"

@event.listens_for(Movie.__table__, 'after_create')
def insert_initial_values_movies(*args, **kwargs):
    db.session.add(Movie('Duro de matar','Acción','Bruce Willis;Alan Rickman', 10.0))
    db.session.add(Movie('Duro de matar 2', 'Acción', 'Bruce Willis;Bonnie Bedelia',10.0))
    db.session.add(Movie('Sexto sentido', 'Suspenso', 'Bruce Willis;Haley Joel Osment;Toni Collette',5.0))
    db.session.add(Movie('Jurassic Park', 'Ciencia ficción', 'Owen Grady;Claire Dearing;John Hammond',15.0))
    db.session.commit()

@event.listens_for(MovieCopy.__table__, 'after_create')
def insert_initial_values_movies_copies(*args, **kwargs):
    db.session.add(MovieCopy(1))
    db.session.add(MovieCopy(1))
    db.session.add(MovieCopy(1))
    db.session.add(MovieCopy(1))
    db.session.add(MovieCopy(2))
    db.session.add(MovieCopy(2))
    db.session.add(MovieCopy(2))
    db.session.add(MovieCopy(3))
    db.session.add(MovieCopy(3))
    db.session.add(MovieCopy(3))
    db.session.add(MovieCopy(4))
    db.session.commit()