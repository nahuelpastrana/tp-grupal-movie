from pika import BlockingConnection, URLParameters
from pika.exceptions import ConnectionClosed
import uuid
import json
from threading import Thread
from datetime import datetime
import logging
import time 
from .exceptions import ExternalServiceUnavailableException

class DeliveryQueuesClient(Thread):
    def init(self, app, url, deliveryQueueName, rentQueueName):
        self.serviceName = 'Delivery Queue'
        self.app = app
        self._paramsConn = URLParameters(url)
        self._connection = None
        self._channelRent = None
        self._channelDelivery = None
        self._rentQueue = None
        self._deliveryQueue = None
        self._rentQueueName = rentQueueName
        self._deliveryQueueName = deliveryQueueName

    def check_connection(self):
        if not (self._connection and self._channelRent.is_open and 
                self._channelRent and self._channelRent.is_open and 
                self._channelDelivery and self._channelDelivery.is_open):
            raise ExternalServiceUnavailableException(self.serviceName)

    def connect(self):
        try:
            needNewConn = (not self._connection or not self._channelRent.is_open)
            if (needNewConn):
                self._connection = BlockingConnection(self._paramsConn)
            
            if(needNewConn or not self._channelRent or not self._channelRent.is_open):
                self._channelRent = self._connection.channel()
                self._rentQueue = self._channelRent.queue_declare(queue=self._rentQueueName, durable=True)
            
            if(needNewConn or  not self._channelDelivery or not self._channelDelivery.is_open):
                self._channelDelivery = self._connection.channel()
                self._deliveryQueue = self._channelDelivery.queue_declare(queue=self._deliveryQueueName, durable=True)
                self._channelDelivery.basic_consume(queue=self._deliveryQueueName, on_message_callback=self.__on_delivery_message)
        except Exception as err:
            raise ExternalServiceUnavailableException(self.serviceName, err)

    def __on_delivery_message(self, channel, method, properties, body):
        if(self.__on_delivery_message_callback is not None):
            channel.basic_ack(delivery_tag = method.delivery_tag)
            self.__on_delivery_message_callback(json.loads(body))

    def __get_rent_body(self, movie_interaction_id, username):
        message = { 
            'uuid': str(uuid.uuid4()),
            'rentId': movie_interaction_id,
            'clientId': username,
            'date_create': datetime.now().timestamp() * 1000 
        }
        return json.dumps(message)

    def __publish_to_rent(self, msg, retry = True):
        try:
            self._channelRent.basic_publish(exchange='', routing_key=self._rentQueueName, body=msg)
        except Exception as err:
            logging.error(str(err))
            if(retry):
                self.connect()
                self.__publish_to_rent(msg, False)
            else:
                raise ExternalServiceUnavailableException(self.serviceName, err)

    def __start_consuming(self):
        while True:
            try:
                self.connect()
                self._channelDelivery.start_consuming()
            except Exception as err:
                if(isinstance(err, ExternalServiceUnavailableException) and err.inner):
                    logging.error(str(err.inner))
                else:
                    logging.error(str(err))
                time.sleep(5)

    def send_rent(self, movie_interaction_id, username):
        body = self.__get_rent_body(movie_interaction_id, username)
        self.__publish_to_rent(body)
     
    def on_delivery_message(self, callback):
        self.__on_delivery_message_callback = callback
    
    def run(self):
        self.__start_consuming()
    
    def stop(self):
        if self._connection and self._connection.is_open:
            self._channelDelivery.stop_consuming()
            self._connection.close()