from flask import Flask, request
import atexit
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask.json import JSONEncoder
from datetime import date
from threading import Thread
from prometheus_flask_exporter import PrometheusMetrics
from .queue_clients import DeliveryQueuesClient
from .api_clients import DeliveryClient, UserClient


db = SQLAlchemy()
deliveryClient = DeliveryClient()
userClient = UserClient()
deliveryQueuesClient = DeliveryQueuesClient()

class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)

def create_app(config="Config"):
    app = Flask(__name__, instance_relative_config=True)
    app.json_encoder = CustomJSONEncoder
    app.config.from_object("config.{}".format(config))
    JWTManager(app)
    PrometheusMetrics(app, buckets=[0.1, 0.4, 0.7])
    from .routes import movies, metrics, histories, healthcheck, route_error  # Import routes
    route_error(app)
    app.register_blueprint(movies)
    app.register_blueprint(metrics)
    app.register_blueprint(histories)
    app.register_blueprint(healthcheck)
    db.init_app(app)
    deliveryClient.init(app.config['DELIVERY_ENDPOINT'])
    userClient.init(app.config['USERS_ENDPOINT'])
    deliveryQueuesClient.init(app, app.config['RABBITMQ_URL'], app.config['DELIVERY_QUEUE'], app.config['RENT_QUEUE'])

    return app
