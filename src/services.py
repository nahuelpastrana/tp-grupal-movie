from calendar import day_name
from datetime import datetime, timedelta
from jsonschema import validate, ValidationError
from . import userClient, deliveryClient, deliveryQueuesClient
from .models import MovieInteraction, MovieCopyStatus, Movie, MovieCopy, MovieInteractionType
from .repositories import movieRepository
from .exceptions import NoStockAvailableException, InsufficientCreditException, NoDeliveryAvailableException, BusinessException, MovieTitleAlreadyExists
from .schemas import schema_movie
from .dtos import MovieCountDTO, DayHourCountDTO, AvailabilityDateDTO, MovieDTO
import logging

class MovieService():
    def __init__(self):
        deliveryQueuesClient.on_delivery_message(self.__on_delivery_message)

    def __noStock(self, movie, username):
        mi = MovieInteraction.createNoStock(movie, username)
        movieRepository.saveChanges([mi])
        raise NoStockAvailableException

    def __insufficientCredit(self, movie, username):
        mi = MovieInteraction.createNoCredit(movie, username)
        movieRepository.saveChanges([mi])
        raise InsufficientCreditException

    def __noDelivery(self, movie, username):
        mi = MovieInteraction.createNoDelivery(movie, username)
        movieRepository.saveChanges([mi])
        raise NoDeliveryAvailableException

    def __processRent(self, movie, movieCopie, username):
        deliveryQueuesClient.check_connection()
        mi = MovieInteraction.createRented(movie, username)
        movieCopie.markAsRented()
        movieRepository.saveChanges([mi, movieCopie])
        deliveryQueuesClient.send_rent(mi.movieInteractionId, username) 
        userClient.processPayment(movie.price)
    
    def __on_delivery_message(self, message):
        with deliveryQueuesClient.app.app_context():
            delivery = message['userId'].upper()
            movieInteractionId = message['rentId']
            parentMi = movieRepository.getInteractionById(movieInteractionId)
            mi = MovieInteraction.createAssignedDeliver(parentMi, delivery)
            movieRepository.saveChanges([mi])

    def registerInquiry(self, movie, username):
        mi = MovieInteraction.createConsulted(movie, username)
        movieRepository.saveChanges([mi])
    
    def getAvailabilityDate(self, movie):
        count = movieRepository.countCopies(movieId=movie.movieId, status=MovieCopyStatus.AVAILABLE)
        if count == 0:
            copies = movieRepository.findCopies(movieId=movie.movieId, status=MovieCopyStatus.RENTED, order_by=MovieCopy.rentedFrom.asc())
            oldestRentalDate = copies[0].rentedFrom
            date = oldestRentalDate + timedelta(days=2)
        else:
            date = datetime.now()
        return AvailabilityDateDTO(date).serialize()
    
    def rent(self, movie, username):
        credit = userClient.getCreditCurrentUser()
        movieCopie = next(filter(lambda x: x.status == MovieCopyStatus.AVAILABLE, movie.copies), None)
        if(movieCopie is None):
            self.__noStock(movie, username)
        if(credit < movie.price):
            self.__insufficientCredit(movie, username)
        if(not deliveryClient.anyDelivery()):
            self.__noDelivery(movie, username)
        self.__processRent(movie, movieCopie, username)
    
    def create(self, dataJson):
        validate(dataJson, schema_movie)

        movieSearchedByTitleCount = movieRepository.count(title=dataJson.get("title"))

        if movieSearchedByTitleCount  > 0:
            raise MovieTitleAlreadyExists

        newMovie = Movie(dataJson.get("title"), dataJson.get("category"), dataJson.get("actor"), dataJson.get("price"))
        newMovie.copies.append(MovieCopy(newMovie.movieId))
        movieRepository.saveChanges([newMovie])

        return MovieDTO(newMovie).serialize()
    
    def getMovieIdAndCountlist(self, movieIdAndCountlist):
        movieIdAndCountlist.sort(key=lambda count: count[1], reverse=True)
        result = movieIdAndCountlist
        return [MovieCountDTO(x[0],x[1]).serialize() for x in result]
    
    def getRentsForDayHours(self, date_from, date_until):
        daysOfWeek = list(day_name) 
        result = []
        for hour in range (0,24):
            rents = movieRepository.findInteractions(type=MovieInteractionType.RENTED, hour=hour, date_from=date_from, date_until=date_until)     
            if len(rents) !=0:
                for day in daysOfWeek:
                    count=0
                    for rent in rents:
                        if rent.date.strftime('%A') == day:
                            count+=1
                    if count != 0:
                        result.append([count,day,hour])

        result = sorted(result,reverse=True)
        return [DayHourCountDTO(x[1],x[2],x[0]).serialize() for x in result]
    
    def getDaysHoursInquiries(self, date_from, date_until):
        daysOfWeek = list(day_name) 
        result = []
        for hour in range (0,24):   
            rents = movieRepository.findInteractions(type=MovieInteractionType.CONSULTED, hour=hour, date_from=date_from, date_until=date_until)  
            if len(rents) !=0:
                for day in daysOfWeek:
                    count=0
                    for rent in rents:
                        if rent.date.strftime('%A') == day:
                            count+=1
                    if count != 0:
                        result.append([count,day,hour])

        result = sorted(result,reverse=True)
        return [DayHourCountDTO(x[1],x[2],x[0]).serialize() for x in result]

movieService = MovieService()