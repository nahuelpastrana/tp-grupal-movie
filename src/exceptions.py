from enum import Enum

class BusinessException(Exception):
    def __init__(self, message):
        self.message=message
        super().__init__(self.message)

class ExternalServiceUnavailableException(Exception):
    def __init__(self, name, inner=None):
        self.message=ErrorMessages.EXTERNAL_SERVICE.format(name)
        self.inner = inner
        super().__init__(self.message)

class MovieTitleAlreadyExists(BusinessException):
    def __init__(self):
        super().__init__(ErrorMessages.MOVIE_TITLE_ALREADY_EXISTS)

class InsufficientCreditException(BusinessException):
    def __init__(self):
        super().__init__(ErrorMessages.NO_CREDIT)

class NoDeliveryAvailableException(BusinessException):
    def __init__(self):
        super().__init__(ErrorMessages.NO_DELIVERY)

class NoStockAvailableException(BusinessException):
    def __init__(self):
        super().__init__(ErrorMessages.NO_STOCK)

class ErrorMessages(str, Enum):
    EXTERNAL_SERVICE='En este momento el servicio \'{}\' no se encuentra disponible, por favor, vuelva a intentarlo luego.'
    MOVIE_TITLE_ALREADY_EXISTS='Ya existe una pelicula con ese título'
    NO_CREDIT='No tiene el crédito suficiente para realizar la transacción'
    NO_DELIVERY='En este momento no se encuentra ningún delivery disponible'
    NO_STOCK='En este momento no se encuentra ninguna copia disponible de la pelicula'
    