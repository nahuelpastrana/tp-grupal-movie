 
schema_movie = {
    "type" : "object",
    "properties" : {
        "title" : {"type" : "string"},
        "category" : {"type" : "string"},
        "actor" : {"type" : "string"},
        "price": {"type" : "number", "minimum": 0},
    },
    "required": ["title","category","actor", "price"]
}
 