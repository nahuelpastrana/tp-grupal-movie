from sqlalchemy import extract, func
from .models import *
from . import db

class MovieRepository():
    def __query(self, title = None, order_by = None):
        query = Movie.query
        if(not title is None):
            query = query.filter(Movie.title==title)
        if(not order_by is None):
            query = query.order_by(order_by)
              
        return query

    def __queryLike(self, title = None, actor = None, category = None, order_by = None):
        query = Movie.query
        if(not title is None):
            query = query.filter(Movie.title.ilike('%' + title + '%'))
        if(not actor is None):
            query = query.filter(Movie.actor.ilike('%' + actor + '%'))
        if(not category is None):
            query = query.filter(Movie.category.ilike('%' + category + '%'))
        if(not order_by is None):
            query = query.order_by(order_by)
              
        return query
    
    def __queryMovieCopy(self, movieId = None, status = None, order_by = None):
        query = MovieCopy.query
        if(not movieId is None):
            query = query.filter(MovieCopy.movieId==movieId)
        if(not status is None):
            query = query.filter(MovieCopy.status==status)
        if(not order_by is None):
            query = query.order_by(order_by)

        return query
    
    def __queryMovieInteraction(self, movieId = None, type = None, date_from = None, date_until = None, hour = None, order_by = None):
        query = MovieInteraction.query
        if(not movieId is None):
            query = query.filter(MovieInteraction.movieId==movieId)
        if(not type is None):
            query = query.filter(MovieInteraction.type==type)
        if(not date_from is None):
            query = query.filter(MovieInteraction.date>=date_from)
        if(not date_until is None):
            query = query.filter(MovieInteraction.date<=date_until)
        if(not hour is None):
            query = query.filter(extract('hour', MovieInteraction.date)==hour)
        if(not order_by is None):
            query = query.order_by(order_by)
        return query

    def getById(self, movie_id):
        return Movie.query.get(movie_id)
    
    def getInteractionById(self, movie_interaction_id):
        return MovieInteraction.query.get(movie_interaction_id)
    
    def saveChanges(self, entities):
        for entity in entities:
            db.session.add(entity)
        db.session.commit()

    def count(self, title = None):
        return self.__query(title=title).count()

    def find(self, title = None, actor = None, category = None, order_by = None):
        return self.__query(title=title, order_by=order_by).all()

    def findLike(self, title = None, actor = None, category = None, order_by = None):
        return self.__queryLike(title=title, actor = actor, category = category, order_by=order_by).all()
    
    def findCopies(self, movieId = None, status = None, order_by = None):
        return self.__queryMovieCopy(movieId=movieId, status=status, order_by=order_by).all()

    def countCopies(self, movieId = None, status = None):
        return self.__queryMovieCopy(movieId=movieId, status=status).count()
    
    def findInteractions(self, movieId = None, type = None, date_from = None, date_until = None, hour = None, order_by = None):
        return self.__queryMovieInteraction(movieId=movieId, type=type, date_from=date_from, date_until=date_until, hour=hour, order_by=order_by).all()
    
    def groupByCountFailedsInteractionsByMovieId(self, date_from = None, date_until = None):
        ListOfFails = [MovieInteractionType.NO_CREDIT, MovieInteractionType.NO_STOCK, MovieInteractionType.NO_DELIVERY]
        query = MovieInteraction.query.with_entities(MovieInteraction.movieId, func.count(MovieInteraction.movieId)).filter(MovieInteraction.date>=date_from, MovieInteraction.date<=date_until)
        query = query.filter(MovieInteraction.type.in_(ListOfFails))
        listCountFailsInteractionsByMovieId = query.group_by(MovieInteraction.movieId).all()
        return listCountFailsInteractionsByMovieId
    
    def groupByCountRentedInteractionsByMovieId(self, date_from = None, date_until = None):
        query = MovieInteraction.query.with_entities(MovieInteraction.movieId, func.count(MovieInteraction.movieId)).filter(MovieInteraction.date>=date_from, MovieInteraction.date<=date_until)
        query = query.filter(MovieInteraction.type==MovieInteractionType.RENTED)
        listCountRentedInteractionsByMovieId = query.group_by(MovieInteraction.movieId).all()
        return listCountRentedInteractionsByMovieId


movieRepository = MovieRepository()