from flask import jsonify
from functools import wraps
from datetime import datetime
from .exceptions import BusinessException

def responseJson(obj, code=200):
    return jsonify(obj), code

def responseMessage(message, code=200):
    return responseJson({"message": message}, code)

def responseNotFound(message = "No se encontró el recurso"):
    return responseMessage(message,404)

def response_error(errors, code):
    return responseJson({"errors": errors}, code)

def responseBusinessError(errors):
    return response_error(errors,400)

def paramToDate(date_string): 
    try:
        return datetime.strptime(date_string, "%Y-%m-%d").date()
    except ValueError:
        raise BusinessException('{} no es una fecha con formato YYYY-MM-DD'.format(date_string))

def only_json(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        from flask import request, abort
        if not request.is_json: 
             abort(415,'Solo se acepta JSON (Content-Type: application/json)')
        else:
            request.json_data = request.get_json()
        return func(*args, **kwargs)
    return wrapper