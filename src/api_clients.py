import requests 
from flask import request
from .exceptions import ExternalServiceUnavailableException

class _HttpClientBase():
    def __init__(self, client_name): 
        self.client_name = client_name
        self.endpoint = None
    
    def init(self, endpoint):
        self.endpoint = endpoint
    
    def getHttpHeaders(self):
        return {
            'Authorization': request.headers.get('Authorization'),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

    def get(self, path):
        url = self.endpoint + path
        try:
            r = requests.get(url=url, headers=self.getHttpHeaders()) 
            r.raise_for_status()
        except:
            raise ExternalServiceUnavailableException(self.client_name)
        return r.json()
    
    def post(self, path, jsonData):
        url = self.endpoint + path
        try:
            r = requests.post(url=url, json=jsonData, headers=self.getHttpHeaders()) 
            r.raise_for_status()
        except:
            raise ExternalServiceUnavailableException(self.client_name)
        return r.json()


class DeliveryClient(_HttpClientBase):
    def __init__(self): 
        super().__init__("Delivery")

    def anyDelivery(self):
        result = super().get("/status")
        return result['status']

class UserClient(_HttpClientBase):
    def __init__(self): 
        super().__init__("Usuarios")
        
    def getCreditCurrentUser(self):
        result = super().get("/credit")
        return result['credit']

    def processPayment(self, price):
        data = {"amount": price * -1}
        result = super().post("/credit", data)
        return result['credit']