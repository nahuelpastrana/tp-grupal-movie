from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, ".env"))

class Config:
    FLASK_APP = environ.get("FLASK_APP")
    FLASK_ENV = environ.get("FLASK_ENV")
    
    JWT_SECRET_KEY = environ.get("JWT_SECRET_KEY")
    JWT_IDENTITY_CLAIM = 'sub'
    
    USERS_ENDPOINT = environ.get("USERS_ENDPOINT")
    DELIVERY_ENDPOINT = environ.get("DELIVERY_ENDPOINT")

    RABBITMQ_URL = environ.get("RABBITMQ_URL")
    DELIVERY_QUEUE = environ.get("DELIVERY_QUEUE")
    RENT_QUEUE = environ.get("RENT_QUEUE")

    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ConfigTest(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI_TEST")