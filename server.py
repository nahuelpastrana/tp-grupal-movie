import atexit
from src import create_app, db, deliveryQueuesClient

app = create_app()

def interrupt():
    global deliveryQueuesClient
    deliveryQueuesClient.stop()

if __name__ == "__main__":
    with app.app_context():
        db.create_all()
    atexit.register(interrupt)
    deliveryQueuesClient.setDaemon(True)
    deliveryQueuesClient.start()
    app.run(host="0.0.0.0", port=7000)
